import time
import os
import re
import sys


class bgcolors:
    WARN = '\033[93m'
    ERROR = '\033[31m'
    DEBUG = '\033[94m'
    INFO = '\033[92m' #Need to figure out stuff
    NORMAL = '\033[0;0m'

def processLine(line):
    infoBool = bool(re.search("\|INFO\|", line))
    errorBool = bool(re.search("\|ERROR\|", line))
    debugBool = bool(re.search("\|DEBUG\|", line))
    warnBool = bool(re.search("\|WARN\|", line))

    if infoBool == True:
        return bgcolors.NORMAL + line
    elif errorBool == True:
        return bgcolors.ERROR + line
    elif debugBool == True:
        return bgcolors.DEBUG + line
    elif warnBool == True:
        return bgcolors.WARN + line
    else:
        return line


def run():
    filename = sys.argv[1]
    file = open(filename, 'r')

    #Find the size of the file and move to the end
    st_results = os.stat(filename)
    st_size = st_results[6]
    file.seek(st_size)

    while 1:
       where = file.tell()
       line = file.readline()
       if not line:
          time.sleep(1)
          file.seek(where)
       else:
          print processLine(line), # already has newline


if __name__ == "__main__":
    run()
